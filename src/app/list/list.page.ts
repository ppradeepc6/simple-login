import { Component, OnInit } from '@angular/core';
import { AuthdataService } from '../services/authdata.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class ListPage implements OnInit {
  userDataArray: any = [];
  hideNshowProgressBar:boolean=false;
  constructor(private authdataService: AuthdataService) { }

  ngOnInit() {
    //fetch all users
    this.fetchAllUserDataDB()
  }
  //ftch user all user Data form db
  async fetchAllUserDataDB() {
    //start loader
    this.hideNshowProgressBar=true;
    const userData = await this.authdataService.fetchUserData("");
    if (userData.isSuccess) {
      this.userDataArray = userData.data;
    }
    //end loader
    this.hideNshowProgressBar= false;
  }
}
