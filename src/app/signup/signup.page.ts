import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { AuthdataService } from '../services/authdata.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  userData: any = {};
  downloadURL: any;
  uploadImageLoader: boolean = false;
  hideNshowProgressBar: boolean = false;
  constructor(
    private authdataService: AuthdataService,
    private toastController: ToastController,
    private router         : Router
  ) {

  }

  ngOnInit() {
  }

  //upload images
  async uploadUserImage(fileInput: any) {
    let self = this;
    const filesToUpload = <Array<File>>fileInput.target.files;
    if (filesToUpload) {
      //show loader
      self.uploadImageLoader = true;
      //uploading Image
      const urldata: any = await self.authdataService.uploadImage(filesToUpload[0]);
      if (urldata.isSuccess) {
        this.downloadURL = urldata.data;
        //hide loader
        self.uploadImageLoader = false;
      } else {
        //show toast of error
        self.presentToast(urldata.data);
        //hide loader
        self.uploadImageLoader = false;
      }
    }
  }

  //creating user
  async signUpUser() {
    try {
      //email validation
      if (this.userData.email == "" || this.userData.email == undefined) {
        this.presentToast(`Email is required`)
        return;
      }

      //used for check email pattern
      const email = this.userData.email.trim();
      const enteredEmail = email.toLowerCase();
      //var emailPattern = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;
      var emailPattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      if (!enteredEmail.match(emailPattern)) {
        //show the toast and return the msg
        this.presentToast("Email not valid");
        return;
      }
      //passord
      if (this.userData.password == "" || this.userData.password == undefined) {
        this.presentToast(`Password is required`)
        return;
      }

      const object = {
        name: this.userData.name ? this.userData.name:"",
        email: email,
        emailLC: enteredEmail,
        password: this.userData.password.trim(),
        address: this.userData.address ? this.userData.address : "",
        imageUrl: this.downloadURL ? this.downloadURL : ""
      }
      //start loader
      this.hideNshowProgressBar= true;
      //call function for chek email is exist or not
      const emaildata = await this.authdataService.fetchUserData({email:enteredEmail});
      if (!emaildata.isSuccess) {
        const userAuthData = await this.authdataService.createAuthUser(object.email, object.password);
        if (userAuthData.isSuccess) {
          object["userId"] = userAuthData.data.user.uid;
          const userData = await this.authdataService.createUserCollection(object);
          if(userData.isSuccess){
            //reset object
            this.userData={};
            this.downloadURL="";
            //stop loader
            this.hideNshowProgressBar= false;
            this.presentToast(`User ${object.email} sign up successfully`);
            //navigate to login page after successfylly signup
            this.loginPage();
          }else{
            this.presentToast(userAuthData.data.message);
            //stop loader
            this.hideNshowProgressBar= false;
          }
        }else{
          this.presentToast(userAuthData.data.message);
          //stop loader
          this.hideNshowProgressBar= false;
        }
      } else {
        this.presentToast("Email already exist");
        //stop loader
        this.hideNshowProgressBar= false;
      }
    } catch (err) {
      this.presentToast(err);
      //stop loader
      this.hideNshowProgressBar= false;
    }
  }

  //navigate to login page
  loginPage(){
    this.router.navigate(["login"])
  }

  //toast for show information
  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 1500
    });
    toast.present();
  }
}
