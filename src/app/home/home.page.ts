import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthdataService } from '../services/authdata.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
private userId :any;
public userData:any;
  constructor(private router: Router,
    private authdataService:AuthdataService) {
      if(this.router.getCurrentNavigation().extras && this.router.getCurrentNavigation().extras.state &&
      this.router.getCurrentNavigation().extras.state.userId){
        this.userId  = this.router.getCurrentNavigation().extras.state.userId;
      }
  }
  ngOnInit() {
    //get user data
    this.fetchUserDataDB();
  }
  //ftch user Data form db
  async fetchUserDataDB(){
    const userData = await this.authdataService.fetchUserData({userId:this.userId});
    if(userData.isSuccess){
      this.userData = userData.data[0];
    }
  }
  //navigate to list page
  openListPage(){
    this.router.navigateByUrl("list")
  }
}
