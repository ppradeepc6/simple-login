import { Injectable } from '@angular/core';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class AuthdataService {
  private STORAGE_DB: any = firebase.storage();
  private FIREBOB_DB: any = firebase.firestore();
  private FIREAUTH  : any = firebase.auth()
  constructor() {
  }

  //promise handle
  returnPromise = (isSuccess, data: any) => ({
    isSuccess,
    data
  })

  //uploadImage
  async uploadImage(imageFile: any) {
    try {
      let self = this;
      let rn = new Date().getTime().toString();
      //Uplaod file
      let snapshot = await self.STORAGE_DB.ref().child(`images/${rn} ${imageFile.name}`).put(imageFile);
      //update
      const downloadURL = await snapshot.ref.getDownloadURL();
      return this.returnPromise(true, downloadURL);
    } catch (error) {
      return this.returnPromise(false, error);
    }
  }

  //check email exist
  async fetchUserData(object) {
    try {
      let  query =  this.FIREBOB_DB.collection("UserCollection")
      //match with email
      if(object.email){
        query =  query.where("emailLC", "==", object.email);
        
      }
      //match with userid
      if(object.userid){
        query =  query.where("userid", "==", object.userid)
        
      }
     const docArry=[]
     const querySnapshot =  await query.get();
      if(!querySnapshot.empty){
        querySnapshot.forEach(doc => {
          docArry.push(doc.data())
        });
        return this.returnPromise(true, docArry);
      }else{
        return this.returnPromise(false, null);
      }
    } catch (error) {
      return this.returnPromise(false, error);
    }
  }

  //create  auth user 
  async createAuthUser(email,password){
    try{
    const userData = await this.FIREAUTH.createUserWithEmailAndPassword(email, password);
    return this.returnPromise(true, userData);
    }catch (error) {
      return this.returnPromise(false, error);
    }
  }
   //ccreate user collection
   async createUserCollection(object) {
    try {
      await this.FIREBOB_DB.collection("UserCollection").doc(object.userId).set(object);
      return this.returnPromise(true, object);
    } catch (error) {
      return this.returnPromise(false, error);
    }
  }
  //login user
  async loginUser(email ,password) {
    try {
      const userData= await this.FIREAUTH.signInWithEmailAndPassword(email, password)
      return this.returnPromise(true, userData);
    } catch (error) {
      return this.returnPromise(false, error);
    }
  }
}
