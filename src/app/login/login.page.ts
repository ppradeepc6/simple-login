import { Component, OnInit } from '@angular/core';
import { AuthdataService } from '../services/authdata.service';
import { ToastController } from '@ionic/angular';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  userData: any = {}
  hideNshowProgressBar: boolean = false;
  constructor(private authdataService: AuthdataService,
    private toastController: ToastController,
    private router : Router) { }

  ngOnInit() {
  }
  //lgin user
  async loginUser() {
    let self = this;
    //show loader
    self.hideNshowProgressBar = true;
    //
    const userData: any = await self.authdataService.loginUser(this.userData.email, this.userData.password);
    if (userData.isSuccess) {
      const userId = userData.data.user.uid
      //reset object
      this.userData={};
      //hide loader
      self.hideNshowProgressBar = false;
      const navigationExtras : NavigationExtras={
        state:{
          userId:userId
        }
      }
      //navigate to home page
      this.router.navigateByUrl("/home",navigationExtras);
    } else {
      //show toast of error
      self.presentToast(userData.data);
      //hide loader
      self.hideNshowProgressBar = false;
    }
  }

   //navigate to sign up page
   signUpPage(){
    this.router.navigate(["signup"])
  }

  //toast for show information
  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 1500
    });
    toast.present();
  }
}
